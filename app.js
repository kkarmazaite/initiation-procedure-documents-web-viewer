let documentJSONArray  = []

function setUI(parameterJSON){
    parameterJSON = JSON.parse(parameterJSON);
    documentJSONArray = parameterJSON.DocumentJSONArray;
    setContent();
}
function setContent(){
    let containerCode = "";
    let fileListItemCode = "";
    documentJSONArray.sort((a,b) => parseFloat(b.Relevant) - parseFloat(a.Relevant));
    documentJSONArray.forEach((documentJSON, i) => {
        fileListItemCode = `
        <div class="file-list-item ${(documentJSON.Relevant==0)? 'not-relevant': ''} ${(documentJSON.IsBeingGenerated==1)? 'is-being-generated': ''}" id='${documentJSON.FileID}-fileListItem' onclick="selectDocument(this.id)">
            <span class="file-list-item-number" id='1-fileListItemNumber'>${i+1}.</span> 
            <span class="file-list-item-name" id='1-fileListItemName'>${documentJSON.DocumentName}</span> 
            <span class="file-list-item-approved" id='1-fileListItemApproved'>${(documentJSON.IsApproved==1)? '<i class="fas fa-check-circle" title="Patvirtintas"></i>': ''}</span> 
            <span class="file-list-item-office365" id='1-fileListItemOffice365'>${(documentJSON.IsInOffise365==1)? '<img title="Dokumentas įkeltas į Office365">': ''}</span> 
        </div>
        `;
        containerCode+=fileListItemCode;
    });
    document.getElementById('file-container').innerHTML= containerCode;
}
function selectDocument(fileID){
    fileID=fileID.slice(0, fileID.indexOf("-"));
    var parameterJSON = '{ "FileID" : "' + fileID + '"}';
    FileMaker.PerformScript("IP Form - Open Document View Card", parameterJSON);
}